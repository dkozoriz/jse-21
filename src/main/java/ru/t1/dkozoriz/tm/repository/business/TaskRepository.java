package ru.t1.dkozoriz.tm.repository.business;

import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends BusinessRepository<Task> implements ITaskRepository {

    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return findAll()
                .stream()
                .filter(t -> projectId.equals(t.getProjectId()))
                .collect(Collectors.toList());
    }

}