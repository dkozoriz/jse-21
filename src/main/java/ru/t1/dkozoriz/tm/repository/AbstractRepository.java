package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.*;

public abstract class AbstractRepository<T extends AbstractModel> implements IAbstractRepository<T> {

    private final Map<String, T> models = new LinkedHashMap<>();

    @Override
    public T add(final T model) {
        models.put(model.getId(), model);
        return model;
    }

    public void addAll(final List<T> model) {
        models.values().addAll(model);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<T> findAll() {
        return new ArrayList<>(models.values());
    }

    public T findById(final String id) {
        return models.get(id);
    }

    public T findByIndex(final Integer index) {
        return models.get(index);
    }

    public int getSize() {
        return models.size();
    }

    public T remove(final T model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    public T removeById(final String id) {
        final T model = findById(id);
        return remove(model);
    }

    public T removeByIndex(final Integer index) {
        final T model = findByIndex(index);
        return remove(model);
    }

    public void removeAll(final Collection<T> collection) {
        collection.stream()
                .map(AbstractModel::getId)
                .forEach(models::remove);
    }

}