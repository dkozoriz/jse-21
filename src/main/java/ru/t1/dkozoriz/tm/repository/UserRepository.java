package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.model.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public User findByLogin(final String login) {
        return findAll()
                .stream()
                .filter(u -> login.equals(u.getLogin()))
                .findFirst()
                .orElse(null);
    }

    public User findByEmail(final String email) {
        return findAll()
                .stream()
                .filter(u -> email.equals(u.getEmail()))
                .findFirst()
                .orElse(null);
    }

    public boolean isLoginExist(final String login) {
        return findAll()
                .stream()
                .anyMatch(u-> login.equals(u.getLogin()));
    }

    public boolean isEmailExist(final String email) {
        return findAll()
                .stream()
                .anyMatch(u-> email.equals(u.getEmail()));
    }

}