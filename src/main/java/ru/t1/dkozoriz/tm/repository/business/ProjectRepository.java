package ru.t1.dkozoriz.tm.repository.business;

import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.model.business.Project;

public final class ProjectRepository extends BusinessRepository<Project> implements IProjectRepository {

}