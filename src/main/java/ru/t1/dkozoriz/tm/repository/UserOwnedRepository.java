package ru.t1.dkozoriz.tm.repository;

import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class UserOwnedRepository<T extends UserOwnedModel> extends AbstractRepository<T>
        implements IUserOwnedRepository<T> {

    public void clear(final String userId) {
        final List<T> userModels = findAll(userId);
        removeAll(userModels);
    }

    public boolean existById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    public List<T> findAll(String userId) {
        if (userId == null) return Collections.emptyList();
        final List<T> result = new ArrayList<>();
        return findAll()
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    public T findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        return findAll()
                .stream()
                .filter(m -> id.equals(m.getId()))
                .filter(m -> userId.equals(m.getUserId()))
                .findFirst().orElse(null);
    }

    public T findByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    public int getSize(final String userId) {
        int count = 0;
        return (int)(findAll()
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count());
    }

    public T removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        final T model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    public T removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) return null;
        final T model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    public T add(final String userId, final T model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    public T remove(final String userId, final T model) {
        if (userId == null || userId.isEmpty()) return null;
        return removeById(userId, model.getId());
    }

}