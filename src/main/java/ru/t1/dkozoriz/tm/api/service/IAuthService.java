package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    User registry(String login, String password, String Email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}
