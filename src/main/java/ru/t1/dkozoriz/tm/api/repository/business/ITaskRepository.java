package ru.t1.dkozoriz.tm.api.repository.business;

import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}