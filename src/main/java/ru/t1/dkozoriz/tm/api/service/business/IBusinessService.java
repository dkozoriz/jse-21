package ru.t1.dkozoriz.tm.api.service.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.service.IUserOwnedService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<T extends BusinessModel> extends IUserOwnedService<T> {

    T removeByIndex(String userId, Integer index);

    T create(String userId, String name, String description);

    List<T> findAll(String userId, Sort comparator);

    T updateById(String userId, String id, String name, String description);

    T updateByIndex(String userId, Integer index, String name, String description);

    T findByIndex(String userId, Integer index);

    T changeStatusById(String userId, String id, Status status);

    T changeStatusByIndex(String userId, Integer index, Status status);

}