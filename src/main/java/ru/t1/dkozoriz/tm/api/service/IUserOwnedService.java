package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.List;

public interface IUserOwnedService<T extends UserOwnedModel> extends IAbstractService<T> {

    void clear(String userId);

    boolean existsById(String userId, String id);

    List<T> findAll(String userId);

    T findById(String userId, String id);

    T findByIndex(String userId, Integer index);

    int getSize(String userId);

    T removeById(String userId, String id);

    T removeByIndex(String userId, Integer index);

    T remove(String userId, T model);
}