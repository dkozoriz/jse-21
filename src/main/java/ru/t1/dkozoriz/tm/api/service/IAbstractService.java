package ru.t1.dkozoriz.tm.api.service;

import java.util.List;

public interface IAbstractService<T> {

    T findById(String id);

    T removeById(String id);

    void clear();

    T add(T user);

    List<T> findAll();

    T remove(T user);

}