package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.List;
import java.util.Optional;

public interface IAbstractRepository<T> {

    interface IRepositoryOptional<T> {

        Optional<T> findById(String id);

        Optional<T> findByIndex(Integer index);

    }

    default IRepositoryOptional<T> optional() {
        final IAbstractRepository<T> repository = this;
        return new IRepositoryOptional<T>() {
            @Override
            public Optional<T> findById(final String id) {
                return Optional.ofNullable(IAbstractRepository.this.findById(id));
            }
            @Override
            public Optional<T> findByIndex(final Integer index) {
                return Optional.ofNullable(IAbstractRepository.this.findByIndex(index));
            }
        };
    }

    List<T> findAll();

    T add(T project);

    void clear();

    T findById(String id);

    int getSize();

    T remove(T project);

    T removeById(String id);

    T findByIndex(Integer index);

    T removeByIndex(Integer index);

}