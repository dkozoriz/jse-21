package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IProjectTaskService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final static String TASK = "task";

    private final static String PROJECT = "project";

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Task bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        final Project project = projectRepository.findById(projectId);
        if (project == null) throw new EntityException(PROJECT);
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new EntityException(TASK);
        task.setProjectId(projectId);
        return task;
    }

    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final Project project = projectRepository.findById(projectId);
        if (project == null) throw new EntityException(PROJECT);
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    public Task unbindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        final Project project = projectRepository.findById(projectId);
        if (project == null) throw new EntityException(PROJECT);
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new EntityException(TASK);
        task.setProjectId(null);
        return task;
    }

}