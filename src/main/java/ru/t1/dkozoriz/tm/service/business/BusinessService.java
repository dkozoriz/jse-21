package ru.t1.dkozoriz.tm.service.business;

import ru.t1.dkozoriz.tm.api.repository.business.IBusinessRepository;
import ru.t1.dkozoriz.tm.api.service.business.IBusinessService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.service.UserOwnedService;

import java.util.List;

public abstract class BusinessService<T extends BusinessModel, R extends IBusinessRepository<T>>
        extends UserOwnedService<T, R> implements IBusinessService<T> {

    public BusinessService(final R abstractRepository) {
        super(abstractRepository);
    }

    public T add(final String userId, final T model) {
        if (model == null) throw new EntityException(getName());
        if (userId == null) throw new UserIdEmptyException();
        model.setUserId(userId);
        return repository.add(model);
    }

    public T changeStatusById(final String userId, final String id, final Status status) {

        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final T model = findById(id);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        return model;
    }

    public T changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        final T model = findByIndex(userId, index);
        if (model == null) throw new EntityException(getName());
        model.setStatus(status);
        return model;
    }

    public T findByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        T model = repository.findByIndex(userId, index);
        if (model == null) throw new EntityException(getName());
        return model;
    }

    public List<T> findAll(final String userId, final Sort sort) {
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

    public T removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    public T updateById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final T model = findById(userId, id);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    public T updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final T model = findByIndex(userId, index);
        if (model == null) throw new EntityException(getName());
        model.setName(name);
        model.setDescription(description);
        return model;
    }

}