package ru.t1.dkozoriz.tm.service.business;

import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends BusinessService<Task, ITaskRepository> implements ITaskService {

    private final static String NAME = "Task";

    @Override
    protected String getName() {
        return NAME;
    }

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    public Task create(final String userId, final String name, final String description, final String projectId) {
        Task task = create(userId, name, description);
        task.setProjectId(projectId);
        return add(userId, task);
    }

    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

}