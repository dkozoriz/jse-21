package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.repository.ICommandRepository;
import ru.t1.dkozoriz.tm.api.service.ICommandService;
import ru.t1.dkozoriz.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}