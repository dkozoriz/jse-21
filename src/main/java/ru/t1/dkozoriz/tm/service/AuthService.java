package ru.t1.dkozoriz.tm.service;

import ru.t1.dkozoriz.tm.api.service.IAuthService;
import ru.t1.dkozoriz.tm.api.service.IUserService;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.user.LoginErrorException;
import ru.t1.dkozoriz.tm.exception.user.PermissionException;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.exception.system.AccessDeniedException;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.Arrays;


public final class AuthService implements IAuthService {

    private final  IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new LoginErrorException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new LoginErrorException();
        if (!hash.equals(user.getPasswordHash())) throw new LoginErrorException();
        userId = user.getId();
    }

    public void logout() {
        userId = null;
    }

    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}