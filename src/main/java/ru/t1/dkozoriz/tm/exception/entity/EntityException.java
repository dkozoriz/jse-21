package ru.t1.dkozoriz.tm.exception.entity;

import ru.t1.dkozoriz.tm.exception.AbstractException;

public final class EntityException extends AbstractException {


    public EntityException(String name) {
        super("Error! " + name + " not found.");
    }

    public EntityException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public EntityException(final Throwable cause) {
        super(cause);
    }

    public EntityException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}