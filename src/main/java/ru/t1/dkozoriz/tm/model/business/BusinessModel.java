package ru.t1.dkozoriz.tm.model.business;

import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.AbstractModel;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.Date;

public abstract class BusinessModel extends UserOwnedModel implements IWBS {

    protected String name = "";

    protected String description = "";

    protected Status status = Status.NOT_STARTED;

    protected Date created = new Date();

    public BusinessModel() {
    }

    public BusinessModel(final String name) {
        this.name = name;
    }

    public BusinessModel(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

}
