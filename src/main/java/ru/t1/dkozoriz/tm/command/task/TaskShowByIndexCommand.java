package ru.t1.dkozoriz.tm.command.task;

import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    private static final String NAME = "task-show-by-index";

    private static final String DESCRIPTION = "show task by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Task task = getTaskService().findByIndex(userId, index);
        showTask(task);
    }

}
