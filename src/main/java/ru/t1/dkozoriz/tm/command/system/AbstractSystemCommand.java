package ru.t1.dkozoriz.tm.command.system;

import ru.t1.dkozoriz.tm.api.service.ICommandService;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected final ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    public Role[] getRoles() {
        return null;
    }

}